library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_unsigned.all;

entity hamcorr is
    port (
        -- TO DO: Make synchronous
        du      : in  std_logic_vector(1 to 7);
        dc      : out std_logic_vector(1 to 7);
        noerror : out std_logic
    );
end hamcorr;

architecture hamcorr of hamcorr is

function syndrome (d:std_logic_vector)
        return std_logic_vector is
    variable syn: std_logic_vector (2 downto 0);
    begin
    syn(0):= d(1) xor d(3) xor d(5) xor d(7);
    syn(1):= d(2) xor d(3) xor d(6) xor d(7);
    syn(2):= d(4) xor d(5) xor d(6) xor d(7);
    return(syn);
end syndrome;

begin
    process(du)
    variable i : integer;
    begin
        dc<=du;
        i:=conv_integer(syndrome(du));
        if i=0 then noerror <='1';
        else noerror <='0'; dc(i) <=not du(i); end if;
    end process;
end hamcorr;