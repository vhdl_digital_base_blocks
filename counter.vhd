library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity counter is
    generic (
        width : in natural := 32
        );
    port (
        rst   : in std_logic;
        clk   : in std_logic;
        load  : in std_logic;
        d     : in std_logic_vector(width-1 downto 0);
        q     : out std_logic_vector(width-1 downto 0)
        );
end entity counter;

architecture rtl of counter is

begin
    process(all) is
    begin
        if rst then
            q <= (others => '0');
        elsif rising_edge(clk) then
            if load then
                q <= d;
            else
                q <= std_logic_vector(unsigned(q) + 1);
            end if;
        end if;
    end process;

end architecture rtl;