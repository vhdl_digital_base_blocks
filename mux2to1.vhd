-- 2 to 1 mux

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;
    use ieee.std_logic_unsigned.all;

entity mux2to1 is
    port (
        a : in std_logic;
        b : in std_logic;
        s : in std_logic;
        y : out std_logic
        );
end mux2to1;

architecture behav1 of mux2to1 is
begin
    process(a, b, s)
    begin
    if (s = '1') then
        y <= a;
    else
        y <= b;
    end if;
    end process;
end behav1;

architecture behav2 of mux2to1 is
begin
    process(a, b, s)
    begin
    case s is
        when '1' =>
            y <= a;
        when others=>
            y <= b;
    end case;
    end process;
end behav2;

architecture behav3 of mux2to1 is
begin
    y <= a when (s = '1') else b;
end behav3;

architecture behav4 of mux2to1 is
begin
    with s select
        y <= a when '1', b when others;
end behav4;