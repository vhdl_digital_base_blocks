library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_unsigned.all;

entity ram_single_port is
generic (
    addr_width : natural := 9; --512x8
    data_width : natural := 8
    );
port (
    addr            : in  std_logic_vector (addr_width - 1 downto 0);
    write_en        : in  std_logic;
    clk             : in  std_logic;
    din             : in  std_logic_vector (data_width - 1 downto 0);
    dout            : out std_logic_vector (data_width - 1 downto 0)
  );
end ram_single_port;

architecture rtl of ram_single_port is

    type mem_type is array ((2** addr_width) - 1 downto 0) of std_logic_vector(data_width - 1 downto 0);
    signal mem : mem_type;

begin
    process (clk)
    begin
        if (rising_edge(clk)) then
            if (write_en = '1') then
                mem(conv_integer(addr)) <= din;
            end if;
            dout <= mem(conv_integer(addr));
        end if;      -- Output register controlled by clock.
    end process;
end rtl;