library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;
    use ieee.std_logic_unsigned.all;

entity bitops is
    port(
        a :  in std_logic_vector(3 downto 0);
        b :  in std_logic_vector(3 downto 0);
        c : out std_logic_vector(3 downto 0);
        d : out std_logic_vector(3 downto 0);
        e : out std_logic_vector(3 downto 0);
        f : out std_logic_vector(3 downto 0);
        g : out std_logic_vector(3 downto 0);
        h : out std_logic_vector(3 downto 0);
        i : out std_logic_vector(3 downto 0);
        j : out std_logic_vector(3 downto 0);
        k : out std_logic_vector(3 downto 0)
        );
end bitops;

architecture behav of bitops is
begin
    c <= a and b;
    d <= a xor b;
    e <= not a;
    f <= a or b;
    g <= a nor b;
    h <= a nand b;
    i <= a xnor b;
    j <= b(3) & a(1 downto 0) & '1';    -- concatentation
    k <= b(2 downto 1) & b(2 downto 1);    -- replication
end behav;