library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity accumulator is
    port (
        rst     : in  std_logic;
        clk     : in  std_logic;
        a       : in  std_logic_vector(3 downto 0);
        accum   : out std_logic_vector(3 downto 0)
    );
end accumulator;

architecture rtl of accumulator is

signal accumL: unsigned(3 downto 0);

begin

    accumulate: process (clk, rst) begin
    if (rst = '1') then
        accumL <= "0000";
    elsif (clk'event and clk= '1') then
        accumL <= accumL + to_unsigned(a);
    end if;
    end process;

    accum <= std_logic_vector(accumL);

end rtl;