library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity bit_synchronizer is
    generic (
        LENGTH : integer := 1
    );
    port (
        clk     : in  std_logic;
        rst     : in  std_logic;
        d       : in  std_logic;
        q       : out std_logic
    );
end entity bit_synchronizer;

architecture arch of bit_synchronizer is

    type PIPE is array (LENGTH - 1 downto 0) of std_logic;

    signal bit_shift : PIPE;

begin

    LENGTH_OFF : if (LENGTH = 0) generate

        q <= d;

    end generate;

    LENGTH_ON : if (LENGTH /= 0) generate

        sync : process( clk, rst )
        begin
            if (rst = '1') then
                bit_shift <= ((others => '0');
            elsif rising_edge(clk) then
                bit_shift <= bit_shift(bit_shift'HIGH - 1 downto 0) & d;
            end if;
        end process;

        q <= bit_shift(bit_shift'HIGH);

    end generate;

end architecture;